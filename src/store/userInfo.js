// @ts-check
import { defineStore } from "pinia";
import { bookListStore } from "./bookList";

export const useUserStore = defineStore("main", {
    state: () => {
        return {
            // all these properties will have their type inferred automatically
            counter: 2,
            name: "Eduardo",
            bookList: [
                "El coronel no tiene quien le escriba",
                "Cien años de soledad",
            ],
            isAdmin: true,
            randomUser: {},
        };
    },
    getters: {
        completedList(state) {
            const wishListStore = bookListStore();
            return state.bookList.concat(wishListStore.wishList);
        },
        // arrow function
        doubleCounter: (state) => {
            return state.counter * 2;
        },
        // function
        squareDoubleCounter() {
            return Math.pow(this.doubleCounter, 2);
        },
    },
    actions: {
        async getRandomUser() {
            fetch("https://randomuser.me/api/")
                .then((response) => {
                    return response.json();
                })
                .then((responseData) => {
                    this.randomUser = responseData["results"][0];
                });

            return this.randomUser;
        },
    },
});
