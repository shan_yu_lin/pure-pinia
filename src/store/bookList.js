import { defineStore } from "pinia";

export const bookListStore = defineStore("wishlist", {
    state: () => {
        return {
            list: ["El jardín de senderos que se bifurcan"]
        };
    },
    getters: {
        wishList: (state) => {
            state.list.push("Aleph")
            return state.list;
        }
    }
});