import Vue from 'vue';
import VueCompositionAPI from '@vue/composition-api';
import App from './App.vue';
import { createPinia, PiniaVuePlugin } from 'pinia';

Vue.use(VueCompositionAPI);
Vue.use(PiniaVuePlugin);
const pinia = createPinia();

new Vue({
  el: '#app',
  pinia,
  render: h => h(App),
});